#include <iostream>

using namespace std;

//kurs: https://main2.edu.pl/main2/courses/show/7/8/
//silnia rekurencyjnie
int silnia (int n){
    if (n == 1)
        return 1;

    return silnia(n-1) * n;
}

//silnia
int main(){
    int n;
    cin >> n;

    cout << silnia(n) << endl;
}
