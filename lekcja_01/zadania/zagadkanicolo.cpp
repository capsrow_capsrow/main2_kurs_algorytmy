#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/tar/
//x^3 + px = q
//x^3 + 3x = 14
//x^3 + 7x = 10
//prawy kraniec przedzualu: x < q/p
//lewy kraniec przedzialu:  x = 1
int main(){
    int n = 0; //n - dlugosc ciagu, ciag musi byc posortowany
    cin >> n;

    long long p[n], q[n];
    for (int i = 0; i < n; ++i)
        cin >> p[i] >> q[i];

    long long lewyzakres;
    long long prawyzakres;
    long long srodek;
    long long wyrazenie;
    long long wynik;
    bool znalezione;

    for (int i = 0; i < n; ++i)
    {
        znalezione = false;
        lewyzakres = 1;
        wynik = 0;
        prawyzakres = q[i] / p[i];

        do{
            //cout << "prawyzakres = " << prawyzakres << endl;
            srodek = (lewyzakres + prawyzakres) / 2;
            //cout << "srodek = " << srodek << endl;
            wyrazenie = (srodek * srodek * srodek) + (p[i] * srodek);
            //cout << "wyrazenie = " << wyrazenie << endl;
            if (wyrazenie == q[i])
            {
                wynik = srodek;
                znalezione = true;
                //cout << "wynik = " << wynik << endl;
            }
            else if (wyrazenie > q[i])
                prawyzakres = srodek - 1;
            else if (wyrazenie < q[i])
            {
                lewyzakres = srodek +1;
                znalezione = true;
            }
        } while (!znalezione);

        if (wynik != 0)
            cout << wynik << endl;
        else
            cout << "BRAK" << endl;
      }
}
