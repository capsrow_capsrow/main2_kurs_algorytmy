#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/akc/

//1. w ciagu wyszukaj liczbe x
//2. jezeli liczba istnieje w ciagu -> wy[x] powieksz o 1
//3. jezeli liczba nie istnieje w ciagu to nie powiekszaj

int wyszukajBinarnieLiczbeWystapienSzukanej (int tablica[], int rozmiartablicy, int szukana)
{
    int poczatek = 0;
    int koniec = rozmiartablicy - 1;
    int srodek;
    int liczbaWystapien = 0;

    //szukamy w lewym przedziale
    //Procedura dzia�a do momentu, a� pocz�tek i koniec przybior� t� sam� warto��.
    //Je�li element x jest w tablicy, musi by� dok�adnie w kom�rce tablica[pocz�tek].
    //Je�li za� w tym miejscu znajduje si� cokolwiek innego ni� x, wiemy, �e nie by�o go w tablicy.
    while (poczatek < koniec)
    {
        srodek = (poczatek + koniec) / 2;
        if (tablica[srodek] >= szukana)     // na �rodku jest element wi�kszy lub r�wny x...
            koniec = srodek;                // zatem x jest w przedziale [poczatek, srodek]
        else                                // wiemy, �e na �rodku jest element mniejszy od x...
            poczatek = srodek + 1;          // zatem x jest w przedziale [srodek+1, koniec]
    }

    int pie = poczatek;                     // w tablica[pocz�tek] jest przechowywane pierwsze wystapienie szukanego elementu
                                            //poniewa� jest to ciag niemalej�cy to mi�dzy [pie] a [koniec] musz� znajdowac si� szukane warto�ci

    poczatek = 0;
    koniec = rozmiartablicy - 1;

    //szukamy w prawym przedziale
    while (poczatek < koniec)
    {
        srodek = ((poczatek + koniec + 1) / 2);// to jest dzielenie przez 2 z zaokr�gleniem w g�r�
        if (tablica[srodek] <= szukana)
        {
            poczatek = srodek;
        }
        else
            koniec = srodek - 1;
    }

    if (tablica[koniec] == szukana)
        liczbaWystapien = koniec - pie + 1;//poniewa� jest to ciag niemalej�cy to mi�dzy [pie] a [koniec] musz� znajdowac si� szukane warto�ci
    else
        liczbaWystapien = 0;

    return liczbaWystapien;
}

int main(){
    int n = 0;  //n - liczba czastek
    cin >> n;

    int v[n];   // v[n] - kolejne predkosci czastek, posortowane niemalejaco
    for (int i = 0; i < n; ++i)
        cin >> v[i];

    int q;      //q - liczba zapytan
    cin >> q;

    int p[q];   //p[q] - predkosci ktore szukamy
    for (int i = 0; i < q; ++i)
        cin >> p[i];

    int liczbaWystapien;
    //na wyjsciu ma byc zaprezentowana czestosc wystepowania szukanych predkosci w ciagu
    for (int i = 0; i < q; ++i)
    {
        liczbaWystapien = wyszukajBinarnieLiczbeWystapienSzukanej(v, n, p[i]);
        cout << liczbaWystapien << endl;
    }

}
