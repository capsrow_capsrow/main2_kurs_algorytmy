#include <iostream>
#include <cstdlib>

using namespace std;

//https://main2.edu.pl/main2/courses/show/7/13/


//dane testowe:
//10
//12 9 99 11 18 7 33 49 7998 15
int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    cin >> n;

    int tablica[n];

    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    for (int k = 0; k < n - 1; ++k)
        for (int i = 0; i < n - k - 1; ++i)
            if (tablica[i] > tablica[i+1])
                swap(tablica[i], tablica[i+1]);

    for (int i = 0; i < n; ++i)
        cout << tablica[i] << " ";

    return 0;
}
