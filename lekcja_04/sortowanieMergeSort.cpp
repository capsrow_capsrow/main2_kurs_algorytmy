#include <iostream>
#include <cstdlib>

using namespace std;

//https://main2.edu.pl/main2/courses/show/7/13/
//opis algorytmu:   http://www.algorytm.org/algorytmy-sortowania/sortowanie-przez-scalanie-mergesort.html
//                  https://eduinf.waw.pl/inf/alg/003_sort/0013.php
//                  https://strefainzyniera.pl/artykul/1069/sortowanie-przez-scalanie
//                  http://math.uni.wroc.pl/~jagiella/p2python/skrypt_html/wyklad5-2.html
//                  https://programowanienaostro.pl/2018/02/08/asynchroniczny-merge-sort/
//wizusalizacja algorytmu
//https://www.youtube.com/watch?v=TzeBrDU-JaY
//https://www.programiz.com/dsa/merge-sort
//https://www.studytonight.com/data-structures/merge-sort#
//https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/lecture-3-insertion-sort-merge-sort/

void MergeSort(int *arr, int p, int q) //indeksy: p -> pocztek zakresu jaki ma zosatc posortowany
                             //         q -> koniec zakresu jaki ma zosatc posortowany
{
    if (p == q) //jak osiagnie koniec przedzialu to juz koniec sortowania
        return;

    int s;
    s = (p + q) / 2;

    MergeSort(arr, p, s);
    MergeSort(arr, s + 1, q);

    int arr_tmp[q];
    int i = p;
    int j = s + 1;

    for (int k = p; k <= q; ++k)
    {
        if (((i <= s) && (arr[i] < arr[j])) || (j > q))
        {
            arr_tmp[k] = arr[i];
            ++i;
        }
        else
        {
            arr_tmp[k] = arr[j];
            ++j;
        }
    }

    for (int k = p; k <= q; ++k)
        arr[k] = arr_tmp[k];
}


//dane testowe:
//10
//12 9 99 11 18 7 33 49 7998 15
int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    cin >> n;

    int tablica[n];

    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    MergeSort(tablica, 0, n -1);

    for (int i = 0; i < n; ++i)
        cout << tablica[i] << " ";

    return 0;
}

