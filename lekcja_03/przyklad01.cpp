#include <iostream>

using namespace std;

//kurs: https://main2.edu.pl/main2/courses/show/7/9/
//"Dany jest ci�g z�o�ony z n liczb ca�kowitych dodatnich. Rozstrzygn��, ile jest jego fragment�w o sumie r�wnej dok�adnie K."
//Pierwszy i oczywisty spos�b:
//sprawd�my wszystkie fragmenty. Przechowajmy ci�g w tablicy A[0..n-1], dla ka�dej pary liczb (i, j) takiej,
//�e i<j, obliczmy sum� A[i]+...+A[j] i sprawd�my, czy jest r�wna K:

int main(){
    int n;
    cin >> n;

    int k;
    cin >> k;

    int a[n];
    for (int i = 0; i < n; ++i)
        cin >> a[i];

    int licznik = 0;                    // tu b�dziemy zlicza� fragmenty o sumie K
    for (int i = 0; i < n; ++i)
        for (int j = i; j < n; ++j)     // podw�jna p�tla - wykona si� dla ka�dej pary (i, j) przy i < j.
        {
            int suma = 0;
            for (int s = i; s <= j; ++s)
                suma += a[s];           // teraz suma = A[i] + ... + A[j]
            if (suma == k)
                licznik++;
        }

    cout << licznik << endl;
}
