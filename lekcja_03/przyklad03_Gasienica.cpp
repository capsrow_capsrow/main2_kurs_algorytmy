#include <iostream>

using namespace std;

//kurs: https://main2.edu.pl/main2/courses/show/7/9/
//wyt�umaczenie algorytmu: https://oki.org.pl/sklad/

int main(){
    int n;
    cin >> n;

    int x;
    cin >> x;

    int a[n];
    for (int i = 0; i < n; ++i)
        cin >> a[i];

    int licznik = 0;        // tu b�dziemy zlicza� fragmenty o sumie x

    int suma = a[0];
    int k = 0;              //wskaznik konca zliczania, on jest ruchomy
    for (int i = 0; i < n; ++i) //i jest w tym wyapadku wskaznikiem poczatku ciagu
    {
        while ((k < n -1) && suma < x)  //przechodze przez kolejnye wyrazy ciagu, ziekszam sume i badam czy jest mniejsza od x
                                        //koncze przechodzenie kiedy k osiaga koniec calego ciagu
        {
            k++;
            suma += a[k];
        }

        if (suma == x)
            licznik++;      //licznik wystapien rownosci do x, jest zwiekszany

        suma -= a[i];       //przed przesunieciem wskaznika poczatku ciagu trzeba odjac od sumy wyraz A[i] bo wskaznikiem przesuwamy sie na A[i+1]
    }

    cout << licznik << endl;
}
