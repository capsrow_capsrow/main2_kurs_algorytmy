#include <iostream>

using namespace std;

int potega (int a, int n){
    if (n == 1)
        return a;
    int s = potega(a, n-1);
    return a*s;
}

int main(){
    int a, n;
    cin >> a;
    cin >> n;
    cout << potega(a, n) << endl;
}
