#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/mij/
//kazda jedynka tworzy tyle par z zerami, ile jest zew w ciagu
//Wczytuj liczby po kolei, przy ka�dej wczytanej jedynce dodawaj do wyniku liczb� dotychczas wczytanych zer.


int main()
{
    unsigned long long n, r, lw = 0, wynik = 0;
    cin >> n;
    for( unsigned long long i = 0; i < n; i++ ) {
        cin >> r;
        if( r == 0 ) {
            ++lw;
            continue;
        }
        wynik += lw;
    }
    cout << wynik << endl;
    return 0;
}
