#include <iostream>

using namespace std;

//kurs: https://main2.edu.pl/main2/courses/show/7/9/
//"Dany jest ci�g z�o�ony z n liczb ca�kowitych dodatnich. Rozstrzygn��, ile jest jego fragment�w o sumie r�wnej dok�adnie K."
//Drugi spos�b:

int main(){
    int n;
    cin >> n;

    int k;
    cin >> k;

    int a[n];
    for (int i = 0; i < n; ++i)
        cin >> a[i];

    int licznik = 0;                    // tu b�dziemy zlicza� fragmenty o sumie K
    for (int i = 0; i < n; ++i)         // dla ustalonego i
    {
        int suma = 0;                   // zacznij od zera
        for (int j = i; j < n; j++)
        {
            suma += a[j];               // w tej chwili suma r�wna jest A[i] + A[i+1] + ... + A[j]
            if (suma == k)
                licznik++;              // i mo�na sprawdzi�, czy jest r�wna K
        }
    }

    cout << licznik << endl;
}
