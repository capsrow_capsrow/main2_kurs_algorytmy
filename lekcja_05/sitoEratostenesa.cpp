#include <bits/stdc++.h>
using namespace std;

//https://main2.edu.pl/main2/courses/show/7/16/
/*
    Najpierw piszemy w rz�dzie wszystkie liczby 2, 3, ..., N, po czym bierzemy najmniejsz� liczb�
    � w tej chwili jest to 2 � i wykre�lamy wszystkie jej wielokrotno�ci (kt�re oczywi�cie nie mog� by� liczbami pierwszymi).
    Teraz bierzemy najmniejsz� liczb�, kt�rej (3), wykre�lamy wszystkie jej wielokrotno�ci, i powtarzamy operacj�, dop�ki to mo�liwe.
    Niewykre�lone liczby, kt�re rozpatrywali�my po drodze (2, 3, 5, ...) to liczby pierwsze, co �atwo udowodni�:
    o ka�dej z nich wiemy, �e nie mog�a by� wielokrotno�ci� �adnej mniejszej liczby, skoro nie zosta�a wcze�niej wykre�lona.
*/

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    cin >> n;

    bool tablica[n+1];              // tablica[j] == false, je�li liczba jest wykre�lona, true je�li nie jest

    for (int i = 0; i <= n; ++i)
        tablica[i] = true;

    for (int i = 2; i <= n; ++i)    // bierzemy kolejna liczbe i
    {
        if (tablica[i])             // jesli nie jest wykreslona
            for (int j = 2 * i; j <= n; j = j + i)  // iterujemy si� po wszystkich wielokrotno�ciach i
                tablica[j] = false;
    }

    cout << "Liczby pierwsze:" << "\n";
    for (int i = 0; i <= n; ++i)
        if (tablica[i])
            cout << i << "\n";

    return 0;
}
