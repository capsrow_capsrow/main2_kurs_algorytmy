#include <iostream>

using namespace std;

//strona: https://main2.edu.pl/main2/courses/show/7/5/

//Ten kod zadzia�a jednak tylko przy za�o�eniu, �e element x znajduje si� w tablicy � po jego zako�czeniu,
//w zmiennej �rodek. Jak zabezpieczy� si� przed sytuacj�, w kt�rej go nie ma?


int main(){
    int n = 0; //n - dlugosc ciagu, ciag musi byc posortowany
    cin >> n;

    int tablica[n];
    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    int x; //liczba do znalezienia
    cin >> x;

    int poczatek = 0;
    int koniec = n - 1;
    int srodek;
    bool znalezione = false;

    do{
        srodek = (poczatek + koniec) / 2;
        if (tablica[srodek] == x)           // trafili�my w x - nie b�dziemy ju� dalej szuka�
            znalezione = true;
        else
            if (tablica[srodek] > x)         // skoro na �rodku jest zbyt du�y element
                koniec = srodek - 1;        // to x jest w przedziale [poczatek, srodek-1]
            else                            // je�li za� na �rodku element jest zbyt ma�y
                poczatek = srodek + 1;      // to szukamy x w przedziale [srodek+1, koniec]
    } while (!znalezione);


    cout << "indeks = " << srodek;
}
