#include <iostream>

using namespace std;

//strona: https://main2.edu.pl/main2/courses/show/7/5/

//Jak zmodyfikowa� procedur� tak, aby znajdowa�a ostatnie wyst�pienie x?
//Wystarczy zapewni�, aby w razie trafienia dok�adnie x kontynuowa� wyszukiwanie w prawej po�owie tablicy.

int main(){
    int n = 0; //n - dlugosc ciagu, ciag musi byc posortowany
    cin >> n;

    int tablica[n];
    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    int x; //liczba do znalezienia
    cin >> x;

    int poczatek = 0;
    int koniec = n - 1;
    int srodek;
    while (poczatek < koniec)
    {
        srodek = (poczatek + koniec + 1) / 2;           // to jest dzielenie przez 2 z zaokr�gleniem w g�r�
        if (tablica[srodek] <= x)
            poczatek = srodek;
        else
            koniec = srodek - 1;
    }

    cout << "indeks = " << srodek;
}
