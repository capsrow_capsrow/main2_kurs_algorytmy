#include <iostream>

using namespace std;

//SZYBKIE POTEGOWANIE

//kurs: https://main2.edu.pl/main2/courses/show/7/8/
//dla parzystych n, liczba a^n jest kwadratem a^n/2,
//na przyk�ad liczb� 2^12 mo�na otrzyma�, podnosz�c do kwadratu liczb� 2^6.

int potega(int a, int n){
    if (n == 1)
        return a;

    if (n % 2 == 0) //wyk�adnik podzielny przez 2
    {
        int s = potega (a, n/2);
        return s * s;
    }
    else
    {
        int s = potega (a, n-1);
        return a * s;
    }

}

int main(){
    int a;
    int n;
    cin >> a;
    cin >> n;

    cout << potega(a, n) << endl;
}
