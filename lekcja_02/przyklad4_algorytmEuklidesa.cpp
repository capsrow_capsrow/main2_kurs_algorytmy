#include <iostream>

using namespace std;

//kurs: https://main2.edu.pl/main2/courses/show/7/8/
//NWD - najwiekszy wspolny dzielnik
//Algorytm Euklidesa
//je�li jaka� liczba d dzieli dwie liczby a i b, dzieli te� ich r�nic� a - b.
//Jest te� w pewnym sensie odwrotnie � je�li wsp�lny dzielnik maj� b oraz a - b, posiada go z ca�� pewno�ci� tak�e a.
//To oznacza, �e zamiast liczb a i b mo�emy r�wnie dobrze wzi�� a - b oraz b
//Na przyk�ad, je�li szukamy wsp�lnego dzielnika dla pary (39,65) mo�emy zamiast tych dw�ch liczb wzi�� 65-39 = 26
//oraz mniejsz� z dw�ch liczb 39.
//Par� (39,26) w ten sam spos�b sprowadzamy do (26,13), potem do (13,13) i ewentualnie do (13,0).
//Najwi�kszy wsp�lny dzielnik liczby i zera to zawsze ta sama liczba � wiemy zatem, �e wynik wynosi 1
//Zauwa�my, �e wynikiem wielokrotnego odejmowania mniejszej liczby b od wi�kszej a, jest ostatecznie
//reszta z dzielenia a przez b (w wy�ej wymienionym przyk�adzie widzimy, �e po odj�ciu 4 odpowiedni� liczb� razy od 10003 otrzymamy 3
//� taki sam wynik, jak gdyby�my od razu podzielili z reszt� 10003 przez 4). Zamiast wi�c liczy� kilka razy r�nic� a-b,
//mo�na od razu przeskoczy� par� krok�w zast�puj�c r�nic� przez wspomnian� reszt�

int nwd (int a, int b){

    if (a < b)
        swap(a, b);
    if (b == 0)
        return a;

    return nwd(b, a % b);
}

int main(){
    int a, b;
    cin >> a;
    cin >> b;

    cout << nwd(a, b) << endl;
}
