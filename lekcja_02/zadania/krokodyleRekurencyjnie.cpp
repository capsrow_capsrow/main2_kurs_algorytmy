#include <iostream>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/kro/
//Poziome linie to poziom. Przecinaj�ce si� linei pionowe i poziome to skrzyzowania.
//Na ka�dym poziomei mog� sie przemie�ci� do a+1 skrzy�owan, i przejsc na nastepny poziom.
//W trakcie przechodzenia moge isc z lewej do prawej, albo z przwej do lewej.
//W trakcie przechidzenia do ostatniego pozuomu miniemy b takich skrzyzowan.
//Czyli jest (a + 1)^b mozliwych przejsc na koniec.

long long potega(int a, int n){
    if (n == 1)
        return a;

    if (n % 2 == 0) //wyk�adnik podzielny przez 2
    {
        long long s = potega (a, n/2);
        return (long long) (s * s);
    }
    else
    {
        long long s = potega (a, n-1);
        return (long long) (a * s);
    }

}

//Potegowanei moduarne: http://smurf.mimuw.edu.pl/node/836
//http://www.algorytm.org/algorytmy-arytmetyczne/szybkie-potegowanie-modularne/spm-c.html
//https://mattomatti.com/pl/a0037
int potegowanieModularne (int a, int b, int m){
    int i;
    int result = 1;
    long int x = a%m;

    for (i=1; i<=b; i<<=1) //i<<=1 - To przesuwa wszystkie bity w liczbie w lewo o jedno miejsce,
                           //a wi�c gdy pocz�tkowym i jest pot�ga liczby 2 (w tym przypadku 1, czyli 2 do zerowej)
                           //to w ka�dej iteracji p�tli b�dzie kolejna pot�ga liczby 2
    {
        x %= m;
        if ((b&i) != 0)   //b&i - zwraca iloczyn bitowy, a wi�c te bity, kt�re powtarzaj� si� w liczbie b i w liczbie i
        {
            result *= x;
            result %= m;
        }
        x *= x;
    }

    return result;
}

int szybkiePotegowanieModulo(int x, int y, int m) {
  int w = x;
  for (int i = 1; i < y; i++)
    w = (w * x) % m;
  return w;
}

int main(){
    int n;
    cin >> n;

    int a[n], b[n];

    for (int i = 0; i < n; ++i)
        cin >> a[i] >> b[i];

    for (int i = 0; i < n; ++i)
        //cout << potega(a[i] + 1, b[i]) % 10000 << endl;
        //cout << potegowanieModularne(a[i] + 1, b[i], 10000) << endl;
        cout << szybkiePotegowanieModulo(a[i] + 1, b[i], 10000) << endl;
}

