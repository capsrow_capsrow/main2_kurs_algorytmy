#include <iostream>
#include <cstdlib>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/inw/

int MergeSort(int *arr, int p, int q) //indeksy: p -> pocztek zakresu jaki ma zosatc posortowany
                             //         q -> koniec zakresu jaki ma zosatc posortowany
{
    if (p == q) //jak osiagnie koniec przedzialu to juz koniec sortowania
        return 0;

    int inwersja;
    int s;
    s = (p + q) / 2;

    MergeSort(arr, p, s);
    MergeSort(arr, s + 1, q);

    int arr_tmp[q];
    int i = p;
    int j = s + 1;

    for (int k = p; k <= q; ++k)
    {
        if (((i <= s) && (arr[i] < arr[j])) || (j > q))
        {
            arr_tmp[k] = arr[i];
            ++i;
            ++inwersja;
        }
        else
        {
            arr_tmp[k] = arr[j];
            ++j;
        }
    }

    for (int k = p; k <= q; ++k)
        arr[k] = arr_tmp[k];

    return inwersja;
}


//dane testowe:
//5
//172 191 179 185 188
int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n;
    cin >> n;

    int tablica[n];
    int inwersja = 0;

    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    inwersja = MergeSort(tablica, 0, n -1);

    for (int i = 0; i < n; ++i)
        cout << tablica[i] << " ";
    cout << "\n";
    cout << inwersja << "\n";

    return 0;
}
