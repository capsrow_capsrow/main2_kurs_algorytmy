#include <iostream>

using namespace std;

//strona: https://main2.edu.pl/main2/courses/show/7/5/

//Zauwa�my, �e w tej wersji nie sprawdzamy za ka�dym razem, czy nie trafili�my w kom�rk� zawieraj�c� x,
//zamiast tego zyskuj�c na prostocie i zwi�z�o�ci algorytmu. Procedura dzia�a do momentu, a� pocz�tek
//i koniec przybior� t� sam� warto��.
//Je�li element x jest w tablicy, musi by� dok�adnie w kom�rce tablica[pocz�tek].
//Je�li za� w tym miejscu znajduje si� cokolwiek innego ni� x, wiemy, �e nie by�o go w tablicy.

//nie jest odporny na sytuacje, je�li w tablicy jest wi�cej ni� jedno wyst�pienie poszukiwanego elementu x

int main(){
    int n = 0; //n - dlugosc ciagu, ciag musi byc posortowany
    cin >> n;

    int tablica[n];
    for (int i = 0; i < n; ++i)
        cin >> tablica[i];

    int x; //liczba do znalezienia
    cin >> x;

    int poczatek = 0;
    int koniec = n - 1;
    int srodek;
    while (poczatek < koniec)
    {
        srodek = (poczatek + koniec) / 2;
        if (tablica[srodek] >= x)           // na �rodku jest element wi�kszy lub r�wny x...
            koniec = srodek;                // zatem x jest w przedziale [poczatek, srodek]
        else                                // wiemy, �e na �rodku jest element mniejszy od x...
            poczatek = srodek + 1;          // zatem x jest w przedziale [srodek+1, koniec]
    }

    cout << "indeks = " << srodek;
}
