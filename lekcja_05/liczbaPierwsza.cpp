#include <bits/stdc++.h>
using namespace std;

bool czy_pierwsza(int n)
{
    for (int i = 2; i * i <= n; ++i)
        if (n % i == 0)     // je�li n ma ma�y dzielnik wi�kszy od 1
            return false;   //to nie jest pierwsza

    return true;            // je�li nie ma ma�ych dzielnik�w, to nie ma �adnych
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    bool wynik;
    int n;
    cin >> n;

    wynik = czy_pierwsza(n);

    if (wynik == false)
    {
        for (int i = 1; i * i <= n; ++i)
            if (n % i == 0)
                cout << "dla i = " << i << " , dzielnik = " << n / i << "\n";
    }

    return 0;
}
