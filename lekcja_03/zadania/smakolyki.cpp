#include <iostream>
#include <cstdlib>

using namespace std;

//zadanie: https://main2.edu.pl/c/konkurs-podstaw-algorytmiki/p/sma/
//vitamin

//

unsigned long long sasiadujace_elementy(int n)
{
    //przykladowo dla liczby 3
    //mozliwe ulozenie:
    //1 2 3
    //12 23
    //123
    //przykladowo dla liczby 4
    //1 2 3 4
    //12 23 34
    //123 234
    //1234
    unsigned long long wynik;

    wynik = n;

    for (int i = 1; i < n; i++)
        wynik += (n - i);


    return wynik;
}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    /* silnia: 1 3 2 -> 3 liczby w ciągu do granicy 2 2
       silnia: 2 3   -> 2 liczby w ciagu od granicy 2 2
       3! + 2!
    5 3
    1 3 2 2 3
    (1) 1          (6) 2
    (2) 1 3        (7) 2
    (3) 1 3 2      (8) 2 3
    (4) 3          (9) 3
    (5) 3 2
    */

    //oki.org.pl - zadanie pociag, tlumaczy algorytm gasienicy

    int n, m;
    unsigned long long licznik = 0;
    int odleglosc = 0;

    cin >> n >> m;

    int cukierek[n];
    int suma_prefix[n];
    for (int i = 0; i < n - 1; i++)
        cin >> cukierek[i];

    for (int i = 0; i < n - 1; i++) //preprocesing: obliczenie sum - wlasciwie to roznic - czastkowych
                                    //od indexu cukierek[n+1] odejmujê cukierek[n]
    {
        suma_prefix[i+1] = abs(cukierek[i+1] - cukierek[i]);
        //cout << "suma_prefix[" << i + 1 << "] = " << suma_prefix[i+1] << "\n";
        if (suma_prefix[i+1] == 0)
        {
            odleglosc = i + 1 - odleglosc;
            licznik += sasiadujace_elementy(odleglosc);
        }
        if (i == (n - 2))
        {
            odleglosc = n -odleglosc;
            licznik += sasiadujace_elementy(odleglosc);
        }
    }



    cout << licznik << "\n";

}
